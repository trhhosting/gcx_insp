import 'package:angular/core.dart';

class FooterData {
  int _id;
  String _title;
  String _subTitle;
  String _copyright;
  String _brand;
  FooterData.data(this._title, this._subTitle, this._copyright, this._brand, [this._id]);
  FooterData();
  factory FooterData.fromJson(Map<String, dynamic> json) => _footerDataFromJson(json);

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }

  //copyright Copyright year
  String get copyright => this._copyright;
  set copyright(String copyright) {
    this._copyright = copyright;
  }

  //brand Company brand ie: Go -> Connectx
  String get brand => this._brand;
  set brand(String brand) {
    this._brand = brand;
  }
}

FooterData _footerDataFromJson(Map<String, dynamic> json) {
  FooterData x = new FooterData();
  x.id = json['id'] as int;
  x.title = json['title'] as String;
  x.subTitle = json['subTitle'] as String;
  x.copyright = json['copyright'] as String;
  x.brand = json['brand'] as String;
  return x;
}

// SocialIcons selection of Social icons that link to user profile
class SocialIconData {
  String _href;
  String _classFormat;
  String _icon;
  String _title;
  int _height;
  int _width;
  SocialIconData.data(this._href, this._classFormat, this._icon, this._height, this._width, [this._title]);
  SocialIconData();
  factory SocialIconData.fromJson(Map<String, dynamic> json) => _socialIconDataFromJson(json);
  //href Address to goto
  String get href => this._href;
  set href(String href) {
    this._href = href;
  }

  //classFormat css classes
  String get classFormat => this._classFormat;
  set classFormat(String classFormat) {
    this._classFormat = classFormat;
  }

  //icon The icon location of the sprite
  String get icon => this._icon;
  set icon(String icon) {
    this._icon = icon;
  }

  //title Button Label
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //height Size of icon height
  int get height => this._height;
  set height(int height) {
    this._height = height;
  }

  //width Size of icon width
  int get width => this._width;
  set width(int width) {
    this._width = width;
  }
}

SocialIconData _socialIconDataFromJson(Map<String, dynamic> json) {
  SocialIconData x = new SocialIconData();
  x.href = json['href'] as String;
  x.classFormat = json['classFormat'] as String;
  x.icon = json['icon'] as String;
  x.title = json['title'] as String;
  x.height = json['height'] as int;
  x.width = json['width'] as int;
  return x;
}

// ResentPosts selection of posts to display no more than 3
class ResentPosts {
  int _id;
  String _title;
  String _date;
  String _href;
  String _classFormat;
  String _photo;
  ResentPosts.data(this._title, this._date, this._href, this._classFormat, this._photo, [this._id]);
  ResentPosts();
  factory ResentPosts.fromJson(Map<String, dynamic> json) => _resentPosts(json);
  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //date
  String get date => this._date;
  set date(String subTitle) {
    this._date = subTitle;
  }

  //href Location that if clicked will take user
  String get href => this._href;
  set href(String href) {
    this._href = href;
  }

  //classFormat Styling for posts
  String get classFormat => this._classFormat;
  set classFormat(String classFormat) {
    this._classFormat = classFormat;
  }

  //photo Image for post
  String get photo => this._photo;
  set photo(String photo) {
    this._photo = photo;
  }
}

ResentPosts _resentPosts(Map<String, dynamic> json) {
  ResentPosts x = new ResentPosts();
  x.title = json['title'] as String;
  x.date = json['date'] as String;
  x.href = json['href'] as String;
  x.classFormat = json['classFormat'] as String;
  x.photo = json['photo'] as String;
  return x;
}

// ContactInfoData Contact information for the client
class ContactInfoData {
  String _href;
  String _classFormat;
  String _title;
  String _lineType;
  ContactInfoData.data(this._href, this._classFormat, this._title, this._lineType);
  ContactInfoData();
  factory ContactInfoData.fromJson(Map<String, dynamic> json) => _contactInfoData(json);
  //href Address to goto
  String get href => this._href;
  set href(String href) {
    this._href = href;
  }

  //classFormat css classes
  String get classFormat => this._classFormat;
  set classFormat(String classFormat) {
    this._classFormat = classFormat;
  }

  //title Button Label
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //lineType Label for what information is stored in the title ie: Name or Address1
  String get lineType => this._lineType;
  set lineType(String lineType) {
    this._lineType = lineType;
  }
}

ContactInfoData _contactInfoData(Map<String, dynamic> json) {
  ContactInfoData x = new ContactInfoData();
  x.href = json['href'] as String;
  x.classFormat = json['classFormat'] as String;
  x.title = json['title'] as String;
  x.lineType = json['lineType'] as String;
  return x;
}
//UsefulLinks Links for menu and outside

class UsefulLinks {
  String _href;
  String _classFormat;
  String _title;
  UsefulLinks.data(this._href, this._classFormat, this._title);
  UsefulLinks();
  factory UsefulLinks.fromJson(Map<String, dynamic> json) => _usefulLinks(json);
  //href Address to goto
  String get href => this._href;
  set href(String href) {
    this._href = href;
  }

  //classFormat css classes
  String get classFormat => this._classFormat;
  set classFormat(String classFormat) {
    this._classFormat = classFormat;
  }

  //title UsefulLinks Label
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }
}

UsefulLinks _usefulLinks(Map<String, dynamic> json) {
  UsefulLinks x = new UsefulLinks();
  x.href = json['href'] as String;
  x.classFormat = json['classFormat'] as String;
  x.title = json['title'] as String;
  return x;
}

//ColumnsData base format for column not to be used directly
@Injectable()
class ColumnData {
  int _id;
  String _title;
  String _subTitle;
  ColumnData.data(this._title, this._subTitle, [this._id]);
  ColumnData();

  //id
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //title
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //subTitle
  String get subTitle => this._subTitle;
  set subTitle(String subTitle) {
    this._subTitle = subTitle;
  }
}

class ColumnOne extends ColumnData {
  List<SocialIconData> _socialIcons;
  ColumnOne.data(this._socialIcons);
  ColumnOne();
  factory ColumnOne.fromJson(Map<String, dynamic> json) => _columnOne(json);
  //socialIcons List of social icon locations
  List<SocialIconData> get socialIcons => this._socialIcons;
  set socialIcons(List<SocialIconData> socialIcons) {
    this._socialIcons = socialIcons;
  }
}

ColumnOne _columnOne(Map<String, dynamic> json) {
  ColumnOne x = new ColumnOne();
  x.id = json['id'] as int;
  x.title = json['title'] as String;
  x.subTitle = json['subTitle'] as String;
  x.socialIcons = _columnOneSocialIconData(json['socialIcons']);
  return x;
}

List<SocialIconData> _columnOneSocialIconData(List<dynamic> json) {
  List<SocialIconData> x = new List<SocialIconData>();
  for (int i = json.length; i >= 1; i--) {
    int id = i - 1;
    SocialIconData dat = new SocialIconData.fromJson(json[id]);
    x.add(dat);
  }
  return x;
}

class ColumnTwo extends ColumnData {
  List<ResentPosts> _posts;
  ColumnTwo.data(this._posts);
  ColumnTwo();
  factory ColumnTwo.fromJson(Map<String, dynamic> json) => _columnTwo(json);

  //posts List of post to display
  List<ResentPosts> get posts => this._posts;
  set posts(List<ResentPosts> posts) {
    this._posts = posts;
  }
}

ColumnTwo _columnTwo(Map<String, dynamic> json) {
  ColumnTwo x = new ColumnTwo();
  x.id = json['id'] as int;
  x.title = json['title'] as String;
  x.subTitle = json['subTitle'] as String;
  x.posts = _columnTwoResentPosts(json["posts"]);
  return x;
}

List<ResentPosts> _columnTwoResentPosts(List<dynamic> json) {
  List<ResentPosts> x = new List<ResentPosts>();
  for (int i = json.length; i >= 1; i--) {
    int id = i - 1;
    ResentPosts dat = new ResentPosts.fromJson(json[id]);
    x.add(dat);
  }
  return x;
}

class ColumnThree extends ColumnData {
  List<ContactInfoData> _contactInfo;
  ColumnThree.data(this._contactInfo);
  ColumnThree();
  factory ColumnThree.fromJson(Map<String, dynamic> json) => _columnThree(json);
  //contactInfo Line items for contact information
  List<ContactInfoData> get contactInfo => this._contactInfo;
  set contactInfo(List<ContactInfoData> contactInfo) {
    this._contactInfo = contactInfo;
  }
}

ColumnThree _columnThree(Map<String, dynamic> json) {
  ColumnThree x = new ColumnThree();
  x.id = json['id'] as int;
  x.title = json['title'] as String;
  x.subTitle = json['subTitle'] as String;
  x.contactInfo = _columnThreeContactInfoData(json['contactInfo']);
  return x;
}

List<ContactInfoData> _columnThreeContactInfoData(List<dynamic> json) {
  List<ContactInfoData> x = new List<ContactInfoData>();
  for (int i = json.length; i >= 1; i--) {
    int id = i - 1;
    ContactInfoData dat = new ContactInfoData.fromJson(json[id]);
    x.add(dat);
  }
  return x;
}

class ColumnFour extends ColumnData {
  List<UsefulLinks> _links;
  ColumnFour.data(this._links);
  ColumnFour();
  factory ColumnFour.fromJson(Map<String, dynamic> json) => _columnFour(json);

  //links List of links
  List<UsefulLinks> get links => this._links;
  set links(List<UsefulLinks> links) {
    this._links = links;
  }
}

ColumnFour _columnFour(Map<String, dynamic> json) {
  ColumnFour x = new ColumnFour();
  x.id = json['id'] as int;
  x.title = json['title'] as String;
  x.subTitle = json['subTitle'] as String;
  x.links = _columnFourUsefulLinks(json['links']);
  return x;
}

List<UsefulLinks> _columnFourUsefulLinks(List<dynamic> json) {
  List<UsefulLinks> x = new List<UsefulLinks>();
  for (int i = json.length; i >= 1; i--) {
    int id = i - 1;
    UsefulLinks dat = new UsefulLinks.fromJson(json[id]);
    x.add(dat);
  }
  return x;
}
