import 'package:angular/core.dart';

//<a href="pricing.html" class="btn btn-common wow fadeInUp" data-wow-delay="0.3s">Add to My Calender</a>

@Injectable()
class Button {
  String _href;
  String _classFormat;
  String _title;
  Button.data(this._href, this._classFormat, this._title);
  Button();
  //href Address to goto
  String get href => this._href;
  set href(String href) {
    this._href = href;
  }

  //classFormat css classes
  String get classFormat => this._classFormat;
  set classFormat(String classFormat) {
    this._classFormat = classFormat;
  }

  //title Button Label
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }
}
