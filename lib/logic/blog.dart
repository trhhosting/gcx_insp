import 'package:angular/core.dart';

import 'cards.dart';

@Injectable()
class Blog {
  String title;
  List<Card> cards;
  Blog(this.cards, this.title);
}

@Injectable()
class BlogMenu {
  String title;
  BlogMenuCards mCards;
  int id;
  BlogMenu(this.title, this.id, this.mCards);
}

class BlogMenuCards {
  Card card1;
  Card card2;
  Card card3;
  Card card4;
  Card card5;
  BlogMenuCards(this.card1, this.card2, this.card3, this.card4, this.card5);
}
