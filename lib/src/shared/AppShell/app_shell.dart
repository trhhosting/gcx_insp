import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_components/app_layout/material_persistent_drawer.dart';
import 'package:angular_components/app_layout/material_temporary_drawer.dart';
import 'package:angular_components/content/deferred_content.dart';
import 'package:angular_components/material_button/material_button.dart';
import 'package:angular_components/material_icon/material_icon.dart';
import 'package:angular_components/material_list/material_list.dart';
import 'package:angular_components/material_list/material_list_item.dart';
import 'package:angular_components/material_toggle/material_toggle.dart';
import 'package:angular_router/angular_router.dart';
import 'package:app/logic/footer.dart';
import 'package:app/logic/icons.dart';
import 'package:http_client/browser.dart';

import '../../routes.dart';
import '../common/SvgIcons/svg_icons.dart';

@Component(
  selector: 'app-shell',
  templateUrl: 'app_shell.html',
  styleUrls: [
    'package:angular_components/app_layout/layout.scss.css',
    'package:angular_components/css/mdc_web/card/mdc-card.scss.css',
    'app_shell.css',
  ],
  directives: [
    routerDirectives,
    coreDirectives,
    DeferredContentDirective,
    MaterialButtonComponent,
    MaterialPersistentDrawerDirective,
    MaterialToggleComponent,
    MaterialListComponent,
    MaterialTemporaryDrawerComponent,
    MaterialListItemComponent,
    MaterialIconComponent,
    SvgIcons,
  ],
  exports: [
    RoutePaths,
    Routes,
  ],
)
class AppShell implements OnInit {
  bool customWidth = false;
  bool end = false;
  bool overlay = false;
  String name = "Go->ConnectX";
  String abr = "GCX";
  String logoIcon = "/static/icons/gcx_sprite/gcx-sprite.svg#gcx-1";
  int logoSize = 50;
  String copyright = DateTime.now().year.toString();
  Element menuSub;
  Element menuBtn;
  Element menuTitle;
  bool menuShow = false;
  ColumnOne One = new ColumnOne();
  ColumnTwo Two = new ColumnTwo();
  ColumnThree Three = new ColumnThree();
  ColumnFour Four = new ColumnFour();
  RouteMenuTop Top = new RouteMenuTop();
  RouteMenuSide Side = new RouteMenuSide();
  IconLogo Logo = new IconLogo();

  Future<void> ngOnInit() async {
    menuSub = await querySelector("#menu-sub");
    menuBtn = await querySelector("#menu-btn");
    menuTitle = await querySelector("#menu-title");
    this.customWidth = false;
    this.end = true;
    this.overlay = false;

    String _footerLogoData = await dataLogo();
    loadLogoData(_footerLogoData);
    String _footerRawData = await dataFooter();
    loadFooterData(_footerRawData);
    String _menuRawData = await dataMenu();
    loadMenuData(_menuRawData);
  }

  void loadFooterData(String data) {
    var x = jsonDecode(data);
    this.One = ColumnOne.fromJson(x['ColumnOne']);
    this.Two = ColumnTwo.fromJson(x['ColumnTwo']);
    this.Three = ColumnThree.fromJson(x['ColumnThree']);
    this.Four = ColumnFour.fromJson(x['ColumnFour']);
  }

  void loadLogoData(String data) {
    var x = jsonDecode(data);
    this.Logo = IconLogo.fromJson(x);
  }

  void loadMenuData(String data) {
    var x = jsonDecode(data);
    this.Side = RouteMenuSide.fromJson(x['RouteMenuSide']);
    this.Top = RouteMenuTop.fromJson(x['RouteMenuTop']);
  }
}

Future<String> dataFooter() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/footer.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> dataMenu() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/menu.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> dataLogo() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/logo.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
