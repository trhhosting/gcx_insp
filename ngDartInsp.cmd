REM Navigation, Logo and Slider
ngdart generate component Airisshane --path lib/src/Insp/Navigation/airisshane
ngdart generate component Zestia --path lib/src/Insp/Logo/zestia
REM Carousel
ngdart generate component Meldin --path lib/src/Insp/Slider/meldin
REM All other homepage section and Elements.
REM Such as: About, Services, Portfolio, Counter, Team, Testimonial, Client Logo, Blog, Pricing, and Contact Us
ngdart generate component Wroognygothor --path lib/src/Insp/About/Wroognygothor
ngdart generate component Ackmard --path lib/src/Insp/Services/Ackmard
ngdart generate component Gametris --path lib/src/Insp/Portfolio/Gametris
ngdart generate component Fhuyraslan --path lib/src/Insp/Counter/Fhuyraslan
ngdart generate component Zigmal --path lib/src/Insp/Team/Zigmal
ngdart generate component Gamud --path lib/src/Insp/Testimonial/Gamud
ngdart generate component Aldarenupdar --path lib/src/Insp/Client/Aldarenupdar
ngdart generate component Kressara --path lib/src/Insp/Logo/Kressara
ngdart generate component Xeniljulthor --path lib/src/Insp/Blog/Xeniljulthor
ngdart generate component Hecton --path lib/src/Insp/Pricing/Hecton
ngdart generate component Kaldar --path lib/src/Insp/ContactUs/Kaldar
ngdart generate component Teressa --path lib/src/Insp/p404/Teressa
ngdart generate component DaigornSabalz --path lib/src/Insp/p422/DaigornSabalz
ngdart generate component Onathe --path lib/src/Insp/p500/Onathe

Rem Components Shared Parts
ngdart generate component Errinaya --path lib/src/Insp/shared/Header/Errinaya
ngdart generate component Mehanderyodan --path lib/src/Insp/shared/Carousel/Mehanderyodan
ngdart generate component BreenElthen --path lib/src/Insp/shared/About/BreenElthen
ngdart generate component Talberonderik --path lib/src/Insp/shared/Welcome/Talberonderik
ngdart generate component Othelenyerpal --path lib/src/Insp/shared/Services/Othelenyerpal
ngdart generate component ModricNaphazw --path lib/src/Insp/shared/Portfolio/ModricNaphazw
ngdart generate component Ahanna --path lib/src/Insp/shared/WorkCounter/Ahanna
ngdart generate component Abardonrelban --path lib/src/Insp/shared/Team/Abardonrelban
ngdart generate component Senic --path lib/src/Insp/shared/Team/Senic
ngdart generate component Akara --path lib/src/Insp/shared/Testimonial/Akara
ngdart generate component KassinaTristan --path lib/src/Insp/shared/Client/KassinaTristan
ngdart generate component ValtaurGreste --path lib/src/Insp/shared/BLog/ValtaurGreste
ngdart generate component Ayne --path lib/src/Insp/shared/ContactUs/Ayne
ngdart generate component Teressa --path lib/src/Insp/shared/Action/Teressa
ngdart generate component Laenaya --path lib/src/Insp/shared/Accirdion/Laenaya
ngdart generate component Luna --path lib/src/Insp/shared/Alert/Luna
ngdart generate component Urda --path lib/src/Insp/shared/Pricing/Urda
ngdart generate component Rahe --path lib/src/Insp/shared/Pricing/Rahe
ngdart generate component Irina --path lib/src/Insp/shared/Pricing/Irina
ngdart generate component Useli --path lib/src/Insp/shared/Tabs/Useli
ngdart generate component Qupar --path lib/src/Insp/shared/Tabs/Qupar
ngdart generate component Vilan --path lib/src/Insp/shared/Tabs/Vilan


REM Pages
ngdart generate component About --path lib/src/Insp/pages/About
ngdart generate component AddProduct --path lib/src/Insp/pages/AddProduct
ngdart generate component BlogPost --path lib/src/Insp/pages/BlogPost
ngdart generate component BlogPosts --path lib/src/Insp/pages/BlogPosts
ngdart generate component ContactUs --path lib/src/Insp/pages/ContactUs
ngdart generate component Discover --path lib/src/Insp/pages/Discover
ngdart generate component Ecommerce --path lib/src/Insp/pages/Ecommerce
ngdart generate component Home --path lib/src/Insp/pages/Home
ngdart generate component Landing --path lib/src/Insp/pages/Landing
ngdart generate component Licenses --path lib/src/Insp/pages/Licenses
ngdart generate component Login --path lib/src/Insp/pages/Login
ngdart generate component P404 --path lib/src/Insp/pages/P404
ngdart generate component P422 --path lib/src/Insp/pages/P422
ngdart generate component P500 --path lib/src/Insp/pages/P500
ngdart generate component Product --path lib/src/Insp/pages/Product
ngdart generate component Profile --path lib/src/Insp/pages/Profile
ngdart generate component Register --path lib/src/Insp/pages/Register
ngdart generate component SearchSidebar --path lib/src/Insp/pages/SearchSidebar
ngdart generate component Settings --path lib/src/Insp/pages/Settings
ngdart generate component TeamInfo --path lib/src/Insp/pages/TeamInfo